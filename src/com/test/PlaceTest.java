package com.test;

import static org.junit.Assert.*;

import javax.servlet.http.HttpSession;

import junit.framework.Assert;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.google.gson.Gson;
import com.models.Connection;
import com.models.SavedPlaces;
import com.models.UserModel;

public class PlaceTest {
 
	UserModel user =new UserModel ();
	
	
	@Test
	public void updatePositionTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "updatePosition";
		String urlParameters = "id=" + 1 + "&lat=" + 33  + "&long=" + 44;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		//fail("Not yet implemented");
		String lat="33";
		String lon="44";
		String id ="1";
		String pName="pizza";
		
		String statusRes = null;
		Boolean status = UserModel.updateUserPosition(Integer.parseInt(id), Double.parseDouble(lat),
				Double.parseDouble(lon), pName);

		Gson gson = new Gson();

		
		statusRes = gson.toJson(status ? 1 : 0);;

		//UserModel test1 = user.login(email, pass);
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	

//////////******************************************not sure 	
	@Test
	public void getLastPositionTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "login";
		String urlParameters = "lat=" + 33 + "&long=" + 34+ "&user_id=" + 1;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
	
		//fail("Not yet implemented");
		String lat="33";
		String lon="44";
		String followerId ="1";
		String place_name="pizza";
		
		String userPosition = null;

		UserModel user = new UserModel();
		Gson gson = new Gson();

		userPosition = gson.toJson(user.getUserDetails(Integer.parseInt(followerId)));

		userPosition= userPosition;;
		
	     Assert.assertEquals(userPosition, retJson);
	     
	}
//////////////***********************************
	@Test
	public void SavePlaceTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "SavePlace";
		String urlParameters = "email=" + "ali@yahoo.com" + "&name=" + "cario";
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
	
		//fail("Not yet implemented");
		String  place_id ="1";
		String newplace="pizza";
		String  user_id ="10";
		
		UserModel user = new UserModel();
		user.savePlace(Integer.parseInt(user_id), Integer.parseInt(place_id));
		JSONObject json = new JSONObject();
		json.put("UserID", user_id);
		json.put("placeId", place_id);
		String statusRes= json.toJSONString();
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	
	/*
	@Test
	public void AddNewPlaceTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "AddNewPlace";
		String urlParameters = "id=" + 1 + "&name=" + "pizza" + "&description=" + "pizzafood" + "&lat=" + 33
				+ "&long=" + 44;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
	
		//fail("Not yet implemented");
		String lat="33";
		String lon="44";
		String place_id ="1";
		String place_name="pizza";
		String place_description ="pizzafood";
		String userId ="10";
		String statusRes=null;
		
		UserModel user = new UserModel();
		user.addNewPlace(Integer.parseInt(userId), place_name, place_description, Double.parseDouble(lat),
				Double.parseDouble(lng), date, time);
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}*/
	
	
	@Test
	public void getAllListSavesPlacesTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "getAllListSavesPlaces";
		String parameter = "email" + "ali@yahoo.com";
		String retJson = Connection.connect(serviceUrl, parameter, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
	
		//fail("Not yet implemented");
		int user_id=1;
		String place_name="pizza";
		String description ="pizzafood";
		String statusRes=null;
		
		String savedPlaces = null;

		UserModel user = new UserModel();
		Gson gson = new Gson();

		savedPlaces = gson.toJson(user.getSavedPlaces());
		statusRes= savedPlaces;
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	

}
