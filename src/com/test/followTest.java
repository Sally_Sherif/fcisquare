package com.test;

import static org.junit.Assert.*;

import javax.servlet.http.HttpSession;

import junit.framework.Assert;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.google.gson.Gson;
import com.models.Connection;
import com.models.FollowerList;
import com.models.UserModel;

public class followTest {
	 
	UserModel user =new UserModel ();
	
	
	@Test
	public void followUserTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "followUser";
		String urlParameters = "FollowerID=" +12 + "&UserID=" + 10;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		//fail("Not yet implemented");
		String user_id="10";
		String follower_id="12";
		
		String statusRes = null;
		UserModel user1 =new UserModel();
		user1.followUser(Integer.parseInt(user_id), Integer.parseInt(follower_id));
		JSONObject json = new JSONObject();
		json.put("UserID", user_id);
		json.put("FollowerID", follower_id);
		statusRes =json.toJSONString();
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	
	
	@Test
	public void UnFollowUserTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "unfollowUser";
		String urlParameters =  "UserID=" + 10 + "&FollowerID=" + 12;		
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		//fail("Not yet implemented");
		String user_id="10";
		String follower_id="12";
		
		String statusRes = null;
		UserModel user1 =new UserModel();
		user1.UnFollowUser(Integer.parseInt(user_id), Integer.parseInt(follower_id));
		JSONObject json = new JSONObject();
		json.put("UserID", Integer.parseInt(user_id));
		statusRes =json.toJSONString();
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	
	@Test
	public void getAllFollowersListTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "showFollow";
		String parameter = "UserID" + 10;
		String retJson = Connection.connect(serviceUrl, parameter, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		//fail("Not yet implemented");
		String user_id="10";
		
		
		String followersList = null;

		UserModel user = new UserModel();
		Gson gson = new Gson();

		followersList = gson.toJson(user.getFollowers());

		 
		String statusRes = followersList;
		
	     Assert.assertEquals(statusRes, retJson);
	     
	}
	
	
	
	
	

}
