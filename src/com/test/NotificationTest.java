package com.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import junit.framework.Assert;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.google.gson.Gson;
//import com.models.CheckList;
import com.models.Connection;
import com.models.UserModel;

public class NotificationTest {

	@Test
	public void MakeCommentTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "MakeComment";
		String urlParameters = "email=" + "ali@yahoo.com" + "&Text=" + "jjj" + "&checkId=" + 1 + "&commentId="
				+ 5;		
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");

		//fail("Not yet implemented");
		String user_id="2";
		String comment_id ="5";
		String  check_inId="1";
		String text="jjj";
		String description ="pizzafood";
		String statusRes=null;
		
		String followedUsersComments = null;

		Gson gson = new Gson();

		followedUsersComments = gson.toJson(UserModel.followedUsersCommentList(Integer.parseInt(user_id)));

	
		
	     Assert.assertEquals(followedUsersComments, retJson);
		
	
	}
	//*****************************
	@Test
	public void MakeLikeTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "MakeLike";
		String urlParameters = "email=" + "ali@yahoo.com" + "&checkId=" + 2 + "&likeId=" + 1;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
	 	//fail("Not yet implemented");
			String user_id="10";
			String  check_in_id="2";
			String like_id="1";
			String statusRes=null;
			
			String followedUsersLikes = null;

			Gson gson = new Gson();

			followedUsersLikes = gson.toJson(UserModel.followedUsersLikeList(Integer.parseInt(user_id)));

			
		     Assert.assertEquals(followedUsersLikes, retJson);
		     
	}
	
	//**********************************
	@Test
	public void getCheckInPlaceTest() {
		String BASE_URL = "http://localhost:8080/FCISquare/rest/";
		String serviceUrl = BASE_URL + "checkInPlace";
		String parameter = "email" + "ali@yahoo.com";
		String retJson = Connection.connect(serviceUrl, parameter, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
	 	//fail("Not yet implemented");
		String user_id="10";
		String  check_in_id="2";
		String like_id="1";
			String statusRes=null;
			
			String userCheckins = null;

			Gson gson = new Gson();

			userCheckins = gson.toJson(UserModel.userCheckins(Integer.parseInt(user_id)));

		
		     Assert.assertEquals(userCheckins, retJson);
		     
	}
	
	
}
