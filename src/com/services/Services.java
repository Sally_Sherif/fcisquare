package com.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.models.FollowerList;
import com.models.SavedPlaces;
import com.models.UserModel;

@Path("/")
public class Services {

	/*
	 * @GET
	 * 
	 * @Path("/signup")
	 * 
	 * @Produces(MediaType.TEXT_HTML) public Response signUp(){ return
	 * Response.ok(new Viewable("/Signup.jsp")).build(); }
	 */

	@POST
	@Path("/signup")
	@Produces("application/json")
	public String signUp(@FormParam("name") String name, @FormParam("email") String email,
			@FormParam("pass") String pass) {

		String userData = null;
		UserModel user = UserModel.addNewUser(name, email, pass);

		Gson gson = new Gson();

		userData = gson.toJson(user);

		return userData;
	}

	// ********************************************************************************************

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String login(@FormParam("email") String email, @FormParam("pass") String pass) {
		String userData = null;

		UserModel user = UserModel.login(email, pass);

		Gson gson = new Gson();

		userData = gson.toJson(user);

		return userData;
	}

	// ***********************************************************************************************
	@POST
	@Path("/updatePosition")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String updatePosition(@FormParam("id") String id, @FormParam("lat") String lat,
			@FormParam("long") String lon,@FormParam("pname") String pName) {

		String statusRes = null;
		Boolean status = UserModel.updateUserPosition(Integer.parseInt(id), Double.parseDouble(lat),
				Double.parseDouble(lon), pName);

		Gson gson = new Gson();

		
		statusRes = gson.toJson(status ? 1 : 0);

		return statusRes;
	}
	// **************************************************************************************

	@POST
	@Path("/followUser")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String followUser(@FormParam("UserID") String user_id, @FormParam("FollowerID") String follower_id) {
		UserModel user = new UserModel();
		user.followUser(Integer.parseInt(user_id), Integer.parseInt(follower_id));
		JSONObject json = new JSONObject();
		json.put("UserID", user_id);
		json.put("FollowerID", follower_id);
		return json.toJSONString();
	}

	// *********************************************************************************************
	@POST
	@Path("/unfollowUser")
	@Produces("application/json")
	public String UnFollowUser(@FormParam("UserID") String user_id, @FormParam("FollowerID") String follower_id) {
		UserModel user = new UserModel();
		user.UnFollowUser(Integer.parseInt(user_id), Integer.parseInt(follower_id));
		JSONObject json = new JSONObject();
		json.put("UserID", Integer.parseInt(user_id));
		json.put("FollowerID", Integer.parseInt(follower_id));
		return json.toJSONString();

	}

	// *************************************************************************************


	// **************************************************************************************
	@POST
	@Path("/savePlace")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String savePlace(@FormParam("UserID") String user_id, @FormParam("placeId") String place_id) {
		UserModel user = new UserModel();
		user.savePlace(Integer.parseInt(user_id), Integer.parseInt(place_id));
		JSONObject json = new JSONObject();
		json.put("UserID", user_id);
		json.put("placeId", place_id);
		return json.toJSONString();
	}
	//******************************************************
	@POST
	@Path("/unSavePlace")
	@Produces("application/json")
	public String unSavePlace(@FormParam("UserID") String user_id, @FormParam("placeId") String place_id) {
		UserModel user = new UserModel();
		user.unSavePlace(Integer.parseInt(user_id), Integer.parseInt(place_id));
		JSONObject json = new JSONObject();
		json.put("UserID", Integer.parseInt(user_id));
		json.put("placeId", Integer.parseInt(place_id));
		return json.toJSONString();

	}
	
	// ******************************************************************************************
	@POST
	@Path("/addNewPlace")
	@Produces("application/json")
	public void addNewPlace(@FormParam("userId") String userId, @FormParam("place_name") String place_name,
			@FormParam("place_description") String place_description, @FormParam("lat") String lat,
			@FormParam("lng") String lng, @FormParam("date") String date, @FormParam("time") String time) {
	
		UserModel user = new UserModel();
		user.addNewPlace(Integer.parseInt(userId), place_name, place_description, Double.parseDouble(lat),
				Double.parseDouble(lng), time, date);
	}

	// ************************************************************************************
	@POST
	@Path("/makeComment")
	@Produces("application/json")
	public void MakeComment(@FormParam("userId") String userId, @FormParam("comment") String comment,
			@FormParam("checkId") String checkId) {
		UserModel user = new UserModel();
		user.makeComment(Integer.parseInt(userId), Integer.parseInt(checkId), comment);
	}

	// **************************************************************************************************************
	@POST
	@Path("/makeLike")
	@Produces("application/json")
	public void MakeLike(@FormParam("userId") String userId, @FormParam("checkId") String checkId) {

		UserModel user = new UserModel();
		user.makeLike(Integer.parseInt(userId), Integer.parseInt(checkId));
	}

	// *****************************************************************************

	@POST
	@Path("/getAllUsers")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String getAllUsers() {
		String allUsers = null;

		UserModel user = new UserModel();
		Gson gson = new Gson();

		allUsers = gson.toJson(user.getAllUsers());

		return allUsers;
	}

	// *********************************************************************************************
	@POST
	@Path("/showUserCheckin")
	@Produces("application/json")
	public String showUserCheckin(@FormParam("UserID") String user_id) {

		String userCheckins = null;

		Gson gson = new Gson();

		userCheckins = gson.toJson(UserModel.userCheckins(Integer.parseInt(user_id)));

		return userCheckins;
	}

	// *********************************************************************************************
	@POST
	@Path("/showLikeList")
	@Produces("application/json")
	public String showLikeList(@FormParam("UserID") String user_id) {

		String followedUsersLikes = null;

		Gson gson = new Gson();

		followedUsersLikes = gson.toJson(UserModel.followedUsersLikeList(Integer.parseInt(user_id)));

		return followedUsersLikes;
	}
	// *************************************************************************************

	@POST
	@Path("/showCommentList")
	@Produces("application/json")
	public String showCommentList(@FormParam("UserID") String user_id) {

		String followedUsersComments = null;

		Gson gson = new Gson();

		followedUsersComments = gson.toJson(UserModel.followedUsersCommentList(Integer.parseInt(user_id)));

		return followedUsersComments;
	}

	// *************************************************************************************
	@POST
	@Path("/checkIn")
	// @Produces("application/json")
	public boolean checkIn(@FormParam("place_name") String place_name, @FormParam("userId") String id,
			@FormParam("date") String date, @FormParam("time") String time) {

		UserModel user = new UserModel();

		if (user.makeCheckIn(Integer.parseInt(id), place_name, time, date)) {
			return true;
		}
		return false;
	}

	// //***********************************************************************
	@POST
	@Path("/placesList")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String getPlaces() {
		String allPlaces = null;

		UserModel user = new UserModel();
		Gson gson = new Gson();

		allPlaces = gson.toJson(user.getAllPlaces());

		return allPlaces;
	}

	
	// //***********************************************************************
		@POST
		@Path("/getPlaceDetails")
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("application/json")
		public String getPlaceDetails(@FormParam("place_id") int placeId) {
			String placeDetails = null;

			UserModel user = new UserModel();
			Gson gson = new Gson();

			placeDetails = gson.toJson(user.getPlaceDetails(placeId));

			return placeDetails;
		}

	//*****************************************************
		@POST
		@Path("/savedPlacesList")
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("application/json")
		public String getSavedPlaces() {
			String savedPlaces = null;

			UserModel user = new UserModel();
			Gson gson = new Gson();

			savedPlaces = gson.toJson(user.getSavedPlaces());

			return savedPlaces;
		}
		//**************************************************
		@POST
		@Path("/getFollowers")
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("application/json")
		public String getFollowersList() {
			String followersList = null;

			UserModel user = new UserModel();
			Gson gson = new Gson();

			followersList = gson.toJson(user.getFollowers());

			return followersList;
		}
		//**************************************************
		@POST
		@Path("/getUserDetails")
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("application/json")
		public String getUserDetailss(@FormParam("follower_id") String followerId) {
			String userDetails = null;

			UserModel user = new UserModel();
			Gson gson = new Gson();

			userDetails = gson.toJson(user.getUserDetails(Integer.parseInt(followerId)));

			return userDetails;
		}

	//*****************************************************
		@POST
		@Path("/getUserLastPosition")
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Produces("application/json")
		public String getUserPosition(@FormParam("follower_id") String followerId) {
			String userPosition = null;

			UserModel user = new UserModel();
			Gson gson = new Gson();

			userPosition = gson.toJson(user.getUserDetails(Integer.parseInt(followerId)));

			return userPosition;
		}
//***********************************************************
	
	/*
	 * @GET
	 * 
	 * @Path("/GetAllNotifications")
	 * 
	 * @Produces("application/json") public String
	 * getAllNotification(@FormParam("email ") String email,
	 * 
	 * @FormParam("time ") String time ,@FormParam("Date") String Date) throws
	 * ClassNotFoundException, SQLException { UserModel use =new UserModel ();
	 * ArrayList<String> arr = new ArrayList<>(); arr =
	 * use.getAllNotification(email
	 * ,Integer.parseInt(time),Integer.parseInt(Date) ); JSONObject json = new
	 * JSONObject(); for(int i=0;i<arr.size();i++) { json.put(
	 * "TypeOfNotification :", arr.get(i)); } JSONArray ja = new JSONArray();
	 * ja.add(json); JSONObject mainObj = new JSONObject();
	 * mainObj.put("TypeOfNotification", ja); return mainObj.toJSONString(); }
	 * //***********************************************************************
	 * *************
	 * 
	 * public String getJson() { return "Hello after editing"; // Connection
	 * URL: // mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/ }
	 * //***********************************************************************
	 * *********
	 * 
	 * }
	 */

}
