package com.models;

public class FollowerList {
	public int user_id;
    public  int follower_id;
    private String follower_name="";
    private String follower_email="";
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getFollower_id() {
		return follower_id;
	}
	public void setFollower_id(int follower_id) {
		this.follower_id = follower_id;
	}
	public String getFollower_name() {
		return follower_name;
	}
	public void setFollower_name(String follower_name) {
		this.follower_name = follower_name;
	}
	public String getFollower_email() {
		return follower_email;
	}
	public void setFollower_email(String follower_email) {
		this.follower_email = follower_email;
	}
    
    
}