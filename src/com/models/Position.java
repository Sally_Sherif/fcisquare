package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Position {

	
	private String user_name;
	private String user_email;
	
	private  Integer UserID;
	private Double latitude;
	private Double longtitude;
	private Integer place_id;
	
	public void set_user_name(String user_name) {
		this.user_name = user_name;
	}
//********************************************************************************************************
	public String get_user_name(){
		return user_name;
	}
//**************************************************************************************************
	public void set_user_email(String user_email) {
		this.user_email= user_email;
	}
//***************************************************************************************
	public String get_user_email(){
		return user_email;
	}
		
//******************************************************************************************
	public void set_User_ID(Integer UserID) {
		this.UserID= UserID;
	}
//********************************************************************************************
	public Integer get_User_ID(){
		return UserID;
	}
//***************************************************************************************************
	public void setlatitude(Double latitude) {
		this.latitude = latitude;
	}
//*************************************************************************************************
	public Double getlatitude() {
		return latitude;
	}
//******************************************************************************************
	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}
//***************************************************************************************
	public Double getlongtitude() {
		return longtitude;
	}
	//*******************************************************************************************
	public void setplaceid(Integer place_id) {
		this.place_id = place_id;
	}
	//**********************************************************************************
	public Integer getplace_id() {
		return place_id;
	}
	//********************************************************************************************
	public static boolean updateUserPosition(Integer id, Double lat, Double lon) {
		try{
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Update users set `lat` = ? , `long` = ? where `id` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setDouble(1, lat);
			stmt.setDouble(2, lon);
			stmt.setInt(3, id);
			stmt.executeUpdate();
			return true;
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	//******************************************************************************************************
	public void getLastPosition(Integer user_id,String place_name){
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT UserID FROM users WHERE UserID = user_id");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT UserID FROM users WHERE UserID = user_id");
	 
			while (rs.next()) {
				
				Integer User_ID = rs.getInt("UserID");
				  System.out.println( User_ID+ "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT name FROM places WHERE name = place_name");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT name FROM places WHERE name = place_name");
	 
			while (rs.next()) {
				
				String placename = rs.getString("name");
				  System.out.println( placename+ "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT lat ,lng FROM places WHERE name = placename");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT lat ,lng FROM places WHERE name = placename");
	 
			while (rs.next()) {
				double lat = rs.getDouble("lat");
				double lon = rs.getDouble("lng");
				  System.out.println(lat + "\n");
				  System.out.println(lon + "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
	}

}
