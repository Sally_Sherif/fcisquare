package com.models;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConnection {
	public static Connection getActiveConnection() {
		/*String host = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
		String port = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
		System.out.println(host);*/
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection connection = null;
			
//			connection = DriverManager
//					.getConnection("jdbc:mysql://127.6.240.2:3306/firstapplication?"
//						+ "user=adminheEHL7l&password=CfPjl8F3H6Ke&characterEncoding=utf8");
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3308/swproject","root","");
			return connection;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
