package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.mysql.jdbc.Statement;

public class UserModel {

	private String name;
	private String placeName;
	private String email;
	private String pass;
	private int id;
	private double lat;
	private double lng;
	
	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}


	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	// *************************************************************************************************************

	public static UserModel login(String email, String pass) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Select * from users where `email` = ? and `password` = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setString(2, pass);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				UserModel user = new UserModel();
				user.id = rs.getInt("id");
				user.email = rs.getString("email");
				user.pass = rs.getString("password");
				user.name = rs.getString("name");
				user.placeName = rs.getString("placeName");
				user.lat = rs.getDouble("lat");
				user.lng = rs.getDouble("lng");
				return user;
			}
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// *********************************************************************************************
	public static UserModel addNewUser(String name, String email, String pass) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Insert into users (`name`,`email`,`password`) VALUES  (?,?,?)";
			// System.out.println(sql);

			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
			stmt.setString(2, email);
			stmt.setString(3, pass);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				UserModel user = new UserModel();
				user.id = rs.getInt(1);
				user.email = email;
				user.pass = pass;
				user.name = name;
				user.lat = 0.0;
				user.lng = 0.0;
				return user;
			}
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// ********************************************************************************************

	public static boolean updateUserPosition(int id, double lat, double lon,String pName ) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = "Update users set  `placeName` = ? ,`lat` = ? , `lng` = ?  where `id` = ?";

			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, pName);
			stmt.setDouble(2, lat);
			stmt.setDouble(3, lon);
			stmt.setInt(4, id);
			stmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	// ************************************************************************************

	public void followUser(Integer user_id, Integer follower_id) {
		try {

			Connection conn = DBConnection.getActiveConnection();
			String sql = ("SELECT f.id as followerID, f.name as FollowerName, u.id as UserID FROM users f join users u on f.id = ? AND u.id = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, follower_id);
			stmt.setInt(2, user_id);

			ResultSet rs = stmt.executeQuery();

			int userID = 0;
			int followerID = 0;
			String followerName = null;

			while (rs.next()) {
				userID = rs.getInt("UserID");
				followerID = rs.getInt("followerID");
				followerName = rs.getString("FollowerName");

				System.out.println(userID + "\n");
			}

			String checkQuery = "SELECT id FROM followerlist WHERE UserID = ? AND FollowerID = ?";

			PreparedStatement checkStmt = conn.prepareStatement(checkQuery);

			checkStmt.setInt(1, user_id);
			checkStmt.setInt(2, follower_id);

			ResultSet checkRS = checkStmt.executeQuery();

			if (!checkRS.next()) {

				String insertFollowQuery = "INSERT INTO followerlist (`FollowerID`,`UserID`,`followername`) VALUES  (?,?,?)";

				PreparedStatement stmt2;
				stmt2 = conn.prepareStatement(insertFollowQuery);

				if (followerID != 0) {
					stmt2.setInt(1, followerID);
					stmt2.setInt(2, userID);
					stmt2.setString(3, followerName);

					stmt2.executeUpdate();
				}
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// ****************************************************************************************
	public void UnFollowUser(int user_id, int follwer_id) {
		try {

			Connection conn = DBConnection.getActiveConnection();
			String sql = ("DELETE FROM followerlist WHERE UserID = ? AND FollowerID = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, user_id);
			stmt.setInt(2, follwer_id);

			stmt.executeUpdate();

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	public ArrayList<UserModel> getAllUsers() {

		ArrayList<UserModel> usersList = new ArrayList<UserModel>();

		try {

			Statement stmt = null;
			Connection conn = DBConnection.getActiveConnection();

			String query = ("SELECT * FROM `users` WHERE id != 10 ORDER BY RAND() LIMIT 5");

			stmt = (Statement) conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				UserModel user = new UserModel();

				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setPass(rs.getString("password"));
				user.setLat(rs.getDouble("lat"));
				user.setLng(rs.getDouble("lng"));

				usersList.add(user);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return usersList;
	}

	// **********************************************************************************************
	

	// ****************************************************************************************
	public void getLastPosition(int user_id, String place_name) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("SELECT UserID FROM users WHERE UserID = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, user_id);
			ResultSet rs = stmt.executeQuery("SELECT UserID FROM users WHERE UserID = ?");

			if (rs.next()) {

				int User_ID = rs.getInt("UserID");
				System.out.println(User_ID + "\n");

			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("SELECT name FROM places WHERE name = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, place_name);
			ResultSet rs = stmt.executeQuery("SELECT name FROM places WHERE name = ?");

			if (rs.next()) {

				String placename = rs.getString("name");
				System.out.println(placename + "\n");

			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("SELECT lat ,long FROM places WHERE name = ? ");
			PreparedStatement stmt;

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, place_name);
			ResultSet rs = stmt.executeQuery("SELECT lat ,long FROM places WHERE name = ?");

			if (rs.next()) {
				double lat = rs.getDouble("lat");
				double lon = rs.getDouble("long");
				System.out.println(lat + "\n");
				System.out.println(lon + "\n");

			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// *************************************************************************************************************
	public void savePlace(int user_id, int place_id) {
		try {

			Connection conn = DBConnection.getActiveConnection();

			String checkQuery = "SELECT id FROM placelist WHERE UserID = ? AND place_id = ?";

			PreparedStatement checkStmt = conn.prepareStatement(checkQuery);

			checkStmt.setInt(1, user_id);
			checkStmt.setInt(2, place_id);

			ResultSet checkRS = checkStmt.executeQuery();

			if (!checkRS.next()) {

				String insertSavingQuery = "INSERT INTO placelist (`UserID`,`place_id`) VALUES  (?,?)";

				PreparedStatement excuteSaving = conn.prepareStatement(insertSavingQuery);
				excuteSaving.setInt(1, user_id);
				excuteSaving.setInt(2, place_id);
				excuteSaving.executeUpdate();
			}

			String noOfSavedPlacesQuery = "SELECT no_saved_places FROM places WHERE id = ?";

			PreparedStatement noOfSavedPlacesStmt = conn.prepareStatement(noOfSavedPlacesQuery);

			noOfSavedPlacesStmt.setInt(1, place_id);

			ResultSet noOfSavedPlacesRS = noOfSavedPlacesStmt.executeQuery();

			if (noOfSavedPlacesRS.next()) {
				int new_no_saved_places = noOfSavedPlacesRS.getInt("no_saved_places") + 1;

				String updateNoOfSavedPlaces = "UPDATE places SET no_saved_places= ? WHERE id= ?";

				PreparedStatement st = conn.prepareStatement(updateNoOfSavedPlaces);
				st.setInt(1, new_no_saved_places);
				st.setInt(2, place_id);
				st.executeUpdate();
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// ***********************************************************************************************************
	public ArrayList<SavedPlaces> getAllListSavesPlaces(int user_id) {
		ArrayList<SavedPlaces> savedplaces = new ArrayList<SavedPlaces>();
		SavedPlaces SavedPlace = new SavedPlaces();
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("SELECT UserID FROM users WHERE UserID = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, user_id);
			ResultSet rs = stmt.executeQuery("SELECT UserID FROM users WHERE UserID = ?");

			if (rs.next()) {
				int id = rs.getInt("UserID");
				System.out.println(id + "\n");

			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		try {
			Connection conn = DBConnection.getActiveConnection();
			// Statement statement = conn.createStatement();
			String sql = "SELECT name FROM placelist WHERE UserID = ?";
			PreparedStatement stmt1;
			stmt1 = conn.prepareStatement(sql);
			stmt1.setInt(1, user_id);

			ResultSet rs = stmt1.executeQuery(sql);

			while (rs.next()) {

				SavedPlace.setid(rs.getInt("UserID"));
				SavedPlace.setNewPlace(rs.getString("name"));

				savedplaces.add(SavedPlace);
			}

			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return savedplaces;
	}

	// ************************************************************************************************************************
	public void makeComment(int userId, int checkId, String comment) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("INSERT INTO commentlist (`checkId`,`UserId`,`Text`) VALUES  (?,?,?)");

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, checkId);
			stmt.setInt(2, userId);
			stmt.setString(3, comment);

			stmt.executeUpdate();

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// ***************************************************************************************************
	public void makeLike(int userId, int checkId) {
		try {
			Connection conn = DBConnection.getActiveConnection();

			String query = ("SELECT likeId FROM likelist WHERE UserID = ? AND checkId = ?");

			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, userId);
			stmt.setInt(2, checkId);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				String sql = ("INSERT into likelist  (`UserID`,`checkId`) VALUES  (?,?)");

				PreparedStatement stmt2 = conn.prepareStatement(sql);
				stmt2.setInt(1, userId);
				stmt2.setInt(2, checkId);

				stmt2.executeUpdate();
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// *************************************************************************************************************
	public ArrayList<NotificationList> respondNotification(int notification_id) {
		ArrayList<NotificationList> notificationlist = new ArrayList<NotificationList>();
		NotificationList notificationlists = new NotificationList();

		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("Select notification_ID from allnotifications where `notification_ID` = ? ");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, notification_id);

			ResultSet rs = stmt
					.executeQuery("Select notification_ID from allnotifications where `notification_ID` = ?");

			if (rs.next()) {
				int Notification_ID = rs.getInt("notification_ID");

				// System.out.println("UserID"+User_ID);
				System.out.print("NotificationID: " + Notification_ID);

			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		try {
			Connection conn = DBConnection.getActiveConnection();
			// Statement statement = conn.createStatement();
			PreparedStatement stmt;
			String sql = "SELECT checkId,likeId,commentId,User_ID FROM allnotifications WHERE notification_ID = ?";
			PreparedStatement stmt1;
			stmt1 = conn.prepareStatement(sql);
			stmt1.setInt(1, notification_id);

			ResultSet rs = stmt1.executeQuery(sql);

			while (rs.next()) {

				// notificationlists.setuser_id(rs.getInt("UserID"));
				notificationlists.set_notification_id(rs.getInt("notification_ID"));
				notificationlists.set_check_in_id(rs.getInt("checkId"));
				notificationlists.set_like_id(rs.getInt("likeId"));
				notificationlists.set_comment_id(rs.getInt("commentId"));
				notificationlists.set_user_id(rs.getInt("User_ID"));

				notificationlist.add(notificationlists);

			}

			rs.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return notificationlist;
	}

	// ************************************************************************************************
	// public ArrayList<UserCheckin> getActions(int user_id) {
	// int value = 0;
	// ArrayList<UserCheckin> checklist = new ArrayList<UserCheckin>();
	// UserCheckin checklists = new UserCheckin();
	//
	// try {
	// Connection conn = DBConnection.getActiveConnection();
	// String sql1 = ("Select UserID from users where `UserID` = ? ");
	// PreparedStatement stmt1;
	// stmt1 = conn.prepareStatement(sql1);
	// stmt1.setInt(1, user_id);
	// ResultSet rs = stmt1.executeQuery("Select UserID from users where
	// `UserID` = ? ");
	//
	// String sql2 = ("Select checkId from likelist where `UserID` = ? ");
	// PreparedStatement stmt2;
	// stmt2 = conn.prepareStatement(sql2);
	// stmt2.setInt(1, user_id);
	//
	// stmt2.executeQuery("Select checkId from likelist where `UserID` = ? ");
	//
	// String sql3 = ("Select checkId from commentlist where `UserID` = ? ");
	// PreparedStatement stmt3;
	// stmt3 = conn.prepareStatement(sql3);
	// stmt3.setInt(1, user_id);
	// stmt3.executeQuery("Select checkId from commentlist where `UserID` = ?
	// ");
	//
	// while (rs.next()) {
	//
	// // notificationlists.setuser_id(rs.getInt("UserID"));
	// // checklists.set_notification_id(rs.getInt("notification_ID"));
	// checklists.set_user_id(rs.getInt("UserID"));
	// checklists.set_check_in_id(rs.getInt("checkId"));
	//
	// value = checklists.get_check_in_id();
	// String sql4 = ("Select UserID,id,timee,Date from checkin where `checkId`
	// = ? ");
	// PreparedStatement stmt4;
	// stmt4 = conn.prepareStatement(sql4);
	// stmt4.setInt(1, user_id);
	// ResultSet rs1 = stmt4.executeQuery("Select UserID,id,timee,Date from
	// checkin where `checkId` = ? ");
	//
	// while (rs1.next()) {
	//
	// checklists.set_user_id(rs1.getInt("UserID"));
	// checklists.set_check_in_id(rs1.getInt("checkId"));
	// checklists.set_place_id(rs1.getInt("id"));
	// checklists.set_time(rs1.getInt("timee"));
	// checklists.set_date(rs1.getInt("Date"));
	//
	// checklist.add(checklists);
	//
	// }
	// }
	// conn.close();
	// } catch (Exception e) {
	// System.err.println("Got an exception! ");
	// System.err.println(e.getMessage());
	// }
	//
	// return checklist;
	//
	// }
	//
	// **********************************************************************
	public static ArrayList<UserCheckin> userCheckins(int user_id) {
		ArrayList<UserCheckin> checkinsList = new ArrayList<UserCheckin>();

		try {
			Connection conn = DBConnection.getActiveConnection();

			String sql = ("Select checkId, users.name as uName, places.name as pName, timee, Date from checkin join places on places.id = checkin.id JOIN users on users.id = checkin.UserID where `UserID` = ? order by no_of_checkins");
			String query = ("SELECT DISTINCT users.name, commentlist.Text, commentlist.commentId FROM checkin JOIN commentlist JOIN users ON users.id = commentlist.UserID WHERE commentlist.checkId = ?");

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setInt(1, user_id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				UserCheckin userCheckin = new UserCheckin();
				List<Comment> commentsList = new ArrayList<Comment>();

				PreparedStatement stmt2 = conn.prepareStatement(query);
				stmt2.setInt(1, rs.getInt("checkId"));

				ResultSet rs2 = stmt2.executeQuery();

				userCheckin.setCheckId(rs.getInt("checkId"));
				userCheckin.setuName(rs.getString("uName"));
				userCheckin.setpName(rs.getString("pName"));
				userCheckin.setTime(rs.getString("timee"));
				userCheckin.setDate(rs.getString("Date"));

				while (rs2.next()) {
					Comment comment = new Comment();

					comment.setCommenterName(rs2.getString("name"));
					comment.setComment(rs2.getString("Text"));
					commentsList.add(comment);

					userCheckin.setCommentsList(commentsList);

				}

				checkinsList.add(userCheckin);
				rs2.close();
				stmt2.close();
			}
			rs.close();
			stmt.close();

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return checkinsList;
	}

	// **********************************************************************
	public static List<FollowedLike> followedUsersLikeList(int user_id) {
		List<FollowedLike> followedLikesList = new ArrayList<FollowedLike>();

		try {
			Connection conn = DBConnection.getActiveConnection();

			String sql = ("SELECT l.name as liker_name, u.name as checker_name, places.name as place_name FROM likelist JOIN followerlist ON followerlist.UserID = likelist.UserID JOIN checkin ON likelist.checkId = checkin.checkId JOIN places ON checkin.id = places.id JOIN users l ON l.id = likelist.UserID join users u ON u.id = checkin.UserID WHERE followerlist.FollowerID = ?");
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setInt(1, user_id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				FollowedLike FollowedLike = new FollowedLike();

				FollowedLike.setLikerName(rs.getString("liker_name"));
				FollowedLike.setCheckerName(rs.getString("checker_name"));
				FollowedLike.setPlaceName(rs.getString("place_name"));

				followedLikesList.add(FollowedLike);
			}

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return followedLikesList;
	}

	// **********************************************************************
	public static List<FollowedComment> followedUsersCommentList(int user_id) {
		ArrayList<FollowedComment> followedCommentsList = new ArrayList<FollowedComment>();

		try {
			Connection conn = DBConnection.getActiveConnection();

			String sql = ("SELECT c.name as commenter_name, u.name as checker_name, places.name as place_name, Text FROM commentlist JOIN followerlist ON followerlist.UserID = commentlist.UserID JOIN checkin ON commentlist.checkId = checkin.checkId JOIN places ON commentlist.checkId = places.id JOIN users c ON c.id = commentlist.UserID join users u ON u.id = checkin.UserID WHERE followerlist.FollowerID = ?");
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setInt(1, user_id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				FollowedComment followedComment = new FollowedComment();

				followedComment.setCheckerName(rs.getString("checker_name"));
				followedComment.setCommenterName(rs.getString("commenter_name"));
				followedComment.setPlaceName(rs.getString("place_name"));
				followedComment.setComment(rs.getString("Text"));

				followedCommentsList.add(followedComment);

			}

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return followedCommentsList;
	}

	// **********************************************************************

	public boolean makeCheckIn(int userId, String placeName, String time, String date) {

		try {
			Connection conn = DBConnection.getActiveConnection();
			String selectPlaceName = "SELECT id, no_of_checkins FROM places WHERE name = ?";
			PreparedStatement stmt = conn.prepareStatement(selectPlaceName);
			stmt.setString(1, placeName);

			ResultSet placeNameIdRS = stmt.executeQuery();

			if (placeNameIdRS.next()) {
				int new_no_of_checkins = placeNameIdRS.getInt("no_of_checkins") + 1;

				String updateNoOfCheckins = "UPDATE places SET no_of_checkins= ? WHERE id= ?";

				PreparedStatement st = conn.prepareStatement(updateNoOfCheckins);
				st.setInt(1, new_no_of_checkins);
				st.setInt(2, placeNameIdRS.getInt("id"));
				st.executeUpdate();

				String insertUserCheckin = "Insert into checkin (UserID,id,timee,Date) VALUES  (?,?,?,?)";

				PreparedStatement stmt1 = conn.prepareStatement(insertUserCheckin);
				stmt1.setInt(1, userId);
				stmt1.setInt(2, placeNameIdRS.getInt("id"));
				stmt1.setString(3, time);
				stmt1.setString(4, date);
				stmt1.executeUpdate();
				return true;
			}
			conn.close();
			return false;

		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	// *************************************************************************************************

	public void addNewPlace(int userId, String placeName, String place_description, double lat, double lng, String time,
			String date) {
		try {
			Connection conn = DBConnection.getActiveConnection();
			String sql = ("Insert into places (`name`,`description`,`lat`,`lng`) VALUES  (?,?,?,?)");
			PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, placeName);
			stmt.setString(2, place_description);
			stmt.setDouble(3, lat);
			stmt.setDouble(4, lng);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {

				String insertUserCheckin = "Insert into checkin (UserID,id,timee,Date) VALUES  (?,?,?,?)";

				PreparedStatement stmt1 = conn.prepareStatement(insertUserCheckin);
				stmt1.setInt(1, userId);
				stmt1.setInt(2, rs.getInt(1));
				stmt1.setString(3, time);
				stmt1.setString(4, date);
				stmt1.executeUpdate();
			}

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}
	// *******************************************************************

	public void unSavePlace(int user_id, int place_id) {
		try {

			Connection conn = DBConnection.getActiveConnection();
			String sql = ("DELETE FROM placelist WHERE UserID = ? AND place_id = ?");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, user_id);
			stmt.setInt(2, place_id);

			stmt.executeUpdate();
			
			String noOfSavedPlacesQuery = "SELECT no_saved_places FROM places WHERE id = ?";

			PreparedStatement noOfSavedPlacesStmt = conn.prepareStatement(noOfSavedPlacesQuery);

			noOfSavedPlacesStmt.setInt(1, place_id);

			ResultSet noOfSavedPlacesRS = noOfSavedPlacesStmt.executeQuery();

			if (noOfSavedPlacesRS.next()) {
				int new_no_saved_places = noOfSavedPlacesRS.getInt("no_saved_places") - 1;

				String updateNoOfSavedPlaces = "UPDATE places SET no_saved_places= ? WHERE id= ?";

				PreparedStatement st = conn.prepareStatement(updateNoOfSavedPlaces);
				st.setInt(1, new_no_saved_places);
				st.setInt(2, place_id);
				st.executeUpdate();
			}

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}
	// *******************************************************

	public List<Place> getAllPlaces() {
		List<Place> placesList = new ArrayList<Place>();

		try {

			Statement stmt = null;
			Connection conn = DBConnection.getActiveConnection();

			String query = ("SELECT * FROM places");

			stmt = (Statement) conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Place place = new Place();

				place.setPlaceId(rs.getInt("id"));
				place.setPlaceName(rs.getString("name"));
				place.setPlaceDescription(rs.getString("description"));
				place.setPlaceLat(rs.getDouble("lat"));
				place.setPlaeLng(rs.getDouble("lat"));
				place.setNoOfCheckins(rs.getInt("no_of_checkins"));
				place.setNoOfSavedPlaces(rs.getInt("no_saved_places"));

				placesList.add(place);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return placesList;

	}
	// ******************************************************

	public Place getPlaceDetails(int placeId) {
		Place place = new Place();
		try {
			Connection conn = DBConnection.getActiveConnection();

			String sql = ("SELECT * FROM places where places.id = ?");

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setInt(1, placeId);

			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {

				place.setPlaceId(rs.getInt("id"));
				place.setPlaceName(rs.getString("name"));
				place.setPlaceDescription(rs.getString("description"));
				place.setPlaceLat(rs.getDouble("lat"));
				place.setPlaeLng(rs.getDouble("lat"));
				place.setNoOfCheckins(rs.getInt("no_of_checkins"));
				place.setNoOfSavedPlaces(rs.getInt("no_saved_places"));
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return place;
	}

	// *********************************************
	public List<PlaceList> getSavedPlaces() {

		List<PlaceList> savedPlaces = new ArrayList<PlaceList>();
		try {
			Connection conn = DBConnection.getActiveConnection();
			Statement stmt = null;
			String sql = ("SELECT places.id as placeId , places.name as placeName, places.description as placeDescription FROM placelist JOIN places ON placelist.place_id = places.id WHERE placelist.UserID = 10");
			// PreparedStatement stmt = conn.prepareStatement(sql);

			// stmt.setInt(1, user_id);
			stmt = (Statement) conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				PlaceList list = new PlaceList();

				list.setPlaceId(rs.getInt("placeId"));
				list.setPlaceName(rs.getString("placeName"));
				list.setPlaceDescription(rs.getString("placeDescription"));

				savedPlaces.add(list);
			}

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return savedPlaces;

	}
	//***********************************************
	
public List<FollowerList> getFollowers() {
		
		List<FollowerList> followers = new ArrayList<FollowerList>();
	try {
		Connection conn = DBConnection.getActiveConnection();
		Statement stmt = null;
		String sql = ("SELECT users.name as followerName , users.email as followerEmail , users.id as followerId FROM users JOIN followerlist ON followerlist.FollowerID = users.id WHERE followerlist.UserID = 10");
		//PreparedStatement stmt = conn.prepareStatement(sql);

		//stmt.setInt(1, user_id);
		stmt = (Statement) conn.createStatement();

		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			FollowerList list = new FollowerList();
			
			list.setFollower_id(rs.getInt("followerId"));
			list.setFollower_name(rs.getString("followerName"));
			list.setFollower_email(rs.getString("followerEmail"));
		

			followers.add(list);
		}

		conn.close();
	} catch (Exception e) {
		System.err.println("Got an exception! ");
		System.err.println(e.getMessage());
	}

	return followers;
	
}
//************************************************************
public UserModel getUserDetails(int followerId) {
	UserModel user = new UserModel();
	try {
		Connection conn = DBConnection.getActiveConnection();

		String sql = ("SELECT * FROM users where users.id = ?");

		PreparedStatement stmt = conn.prepareStatement(sql);

		stmt.setInt(1, followerId);

		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {

			user.setId(rs.getInt("id"));
			user.setName(rs.getString("name"));
			user.setPlaceName(rs.getString("placeName"));
			user.setEmail(rs.getString("email"));
			user.setLat(rs.getDouble("lat"));
			user.setLng(rs.getDouble("lng"));
			//System.out.println(user.getEmail());
			
		}
		conn.close();
	} catch (Exception e) {
		System.err.println("Got an exception! ");
		System.err.println(e.getMessage());
	}
	return user;
}

// *********************************************


// *********************************************


}