package com.models;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Test {
	
	@POST
	@Path("/collectUsersActions")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/json")
	public String getAllListSavesPlaces(@FormParam("UserID") int user_id) {
		SavedPlaces place=new SavedPlaces();
		UserModel user =new UserModel();
		user.getAllListSavesPlaces(user_id);
		JSONObject json = new JSONObject();
		json.put("UserID", user_id);
		json.put("name",place.getNewPlace());
		
		JSONArray ja = new JSONArray();
		ja.add(json);
		JSONObject mainObj = new JSONObject();
		mainObj.put("placelist", ja);
		return mainObj.toJSONString();
	}
		
}