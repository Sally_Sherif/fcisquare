package com.models;

public class Place {

	private int placeId;
	private String placeName;
	private String placeDescription;
	private double placeLat;
	private double plaeLng;
	private int noOfSavedPlaces;
	private int noOfCheckins;

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPlaceDescription() {
		return placeDescription;
	}

	public void setPlaceDescription(String placeDescription) {
		this.placeDescription = placeDescription;
	}

	public double getPlaceLat() {
		return placeLat;
	}

	public void setPlaceLat(double placeLat) {
		this.placeLat = placeLat;
	}

	public double getPlaeLng() {
		return plaeLng;
	}

	public void setPlaeLng(double plaeLng) {
		this.plaeLng = plaeLng;
	}

	public int getNoOfSavedPlaces() {
		return noOfSavedPlaces;
	}

	public void setNoOfSavedPlaces(int noOfSavedPlaces) {
		this.noOfSavedPlaces = noOfSavedPlaces;
	}

	public int getNoOfCheckins() {
		return noOfCheckins;
	}

	public void setNoOfCheckins(int noOfCheckins) {
		this.noOfCheckins = noOfCheckins;
	}

}