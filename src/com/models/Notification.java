package com.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.Statement;

public class Notification {
	private int user_id;
	private int comment_id;
	private int like_id;
	private String Text="";
	private int check_id;
	
	public void set_user_id(int user_id){
		this.user_id=user_id;
	}
	//****************************************************************
	public int get_user_id(){
		return user_id;
	}
	//****************************************************************
	public void set_comment_id(int comment_id){
		this.comment_id=comment_id;
	}
	//**********************************************************
	public int get_comment_id(){
		return comment_id;
	}
	//*****************************************************************
	public void set_text(String Text){
		this.Text=Text;
	}
	//*************************************************************
	public String get_Text(){
		return Text;
	}
	//********************************************************************************
	public void set_like_id(int like_id){
		this.like_id=like_id;
	}
	//*************************************************************
	public int get_like_id(){
		return like_id;
	}
	//************************************************************************
	public void set_check_id(int check_id){
		this.check_id=check_id;
	}
	//*************************************************************
	public int get_check_id(){
		return check_id;
	}
	//**********************************************************************************
	public void MakeComment(String email,String Text,Integer check_in_id,Integer Comment_id){
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT email FROM users WHERE email = email");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT email FROM users WHERE email = email");
	 
			while (rs.next()) {
				String mail = rs.getString("email");
				  System.out.println(mail + "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT checkId FROM checkin WHERE checkId =  check_in_id");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT checkId FROM checkin WHERE checkId =  check_in_id");
	 
			while (rs.next()) {
				int check_in_ID = rs.getInt("checkId");
				  System.out.println(check_in_ID + "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("INSERT INTO commentlist " + "VALUES (Comment_id,check_in_ID,mail,Text)");
		Statement stmt = (Statement) conn.createStatement();

		stmt.executeUpdate("INSERT INTO commentlist " + "VALUES (Comment_id,check_in_ID,mail,Text)");
		conn.close();
	} catch (Exception e) {
	    System.err.println("Got an exception! ");
	    System.err.println(e.getMessage());
	}
		System.out.println("User Added Comment on This Post");
		
	}

	//***************************************************************************************************
	public void MakeLike(String email,Integer check_in_id,Integer Like_id){
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT email FROM users WHERE email = email");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT email FROM users WHERE email = email");
	 
			while (rs.next()) {
				String mail = rs.getString("email");
				  System.out.println(mail + "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("SELECT checkId FROM checkin WHERE checkId =  check_in_id");
			PreparedStatement stmt;
			stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery("SELECT checkId FROM checkin WHERE checkId =  check_in_id");
	 
			while (rs.next()) {
				int check_in_ID = rs.getInt("checkId");
				  System.out.println(check_in_ID + "\n");

		
	}
			conn.close();
		} catch (Exception e) {
	        System.err.println("Got an exception! ");
	        System.err.println(e.getMessage());
	    }
		
		try{
		Connection conn = DBConnection.getActiveConnection();
		String sql=("INSERT INTO likelist " + "VALUES (  Like_id,mail,check_in_ID)");
		Statement stmt = (Statement) conn.createStatement();

		stmt.executeUpdate("INSERT INTO likelist " + "VALUES (  Like_id,mail,check_in_ID)");
		conn.close();
	} catch (Exception e) {
	    System.err.println("Got an exception! ");
	    System.err.println(e.getMessage());
	}
		System.out.println("User Added Like on This Post");
		
	}
}
