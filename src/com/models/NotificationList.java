package com.models;

public class NotificationList {
	public int notification_id;
	public int like_id;
	public int comment_id;
	public int user_id;
	public int check_in_id;
	public void set_notification_id(int notification_id){
		this.notification_id=notification_id;
	}
	public void set_like_id(int like_id){
		this.like_id=like_id;
	}
	public void set_comment_id(int comment_id){
		this.comment_id=comment_id;
	}
	public void set_check_in_id(int check_in_id){
		this.check_in_id=check_in_id;
	}
	public void set_user_id(int user_id){
		this.user_id=user_id;
	}
	//**************************************************************************************
	public int get_notification_id(){
		return notification_id;
	}
	
	public int comment_id(){
		return comment_id;
	}
	public int get_like_id(){
		return like_id;
	}
	public int get_user_id(){
		return user_id;
	}
	public int get_check_in_id(){
		return check_in_id;
	}
	

}
